#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
MARKER_FILE="/usr/local/etc/vagrant_provision_marker"

TOOLS_FOLDER="/vagrant/tools"
JLINK="JLink_Linux_V634f_x86_64.deb"
NRF_CLI="nRF5x-Command-Line-Tools_9_7_3_Linux-x86_64.tar"
NRF_SDK="nRF5_SDK_15.2.0_9412b96"
GNU_ARM="gcc-arm-none-eabi-7-2018-q2-update"
GNU_ARM_FILE="gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2"

# Only provision once
#if [ -f "${MARKER_FILE}" ]; then
#    exit 0
#fi

# Update get
apt-get update --quiet

# Install basic dependencies
apt-get install -qy build-essential git curl tar unzip

# Install OpenOCD
apt-get install -qy openocd

# Install JLink software
apt-get install -qy ${TOOLS_FOLDER}/${JLINK}

# Install the GNU Arm Embedded Toolchain
if [ ! -d /usr/local/bin/${GNU_ARM} ]; then
    tar xjf /vagrant/tools/${GNU_ARM_FILE} -C /usr/local/bin
fi
grep -q "export PATH="$PATH:/usr/local/bin/${GNU_ARM}/bin"" /home/vagrant/.profile \
    || echo "export PATH="$PATH:/usr/local/bin/${GNU_ARM}/bin"" >> /home/vagrant/.profile

# Extract the nRF5 CLI
tar xf ${TOOLS_FOLDER}/${NRF_CLI} -C /opt 
if [ ! -f /usr/local/bin/nrfjprog ]; then
    sudo ln -s /opt/nrfjprog/nrfjprog /usr/local/bin/nrfjprog
fi
if [ ! -f /usr/local/bin/mergehex ]; then
    sudo ln -s /opt/mergehex/mergehex /usr/local/bin/mergehex
fi

# Extract the SDK and export the NRF5 SDK path
if [ ! -d /vagrant/${NRF_SDK} ]; then
    unzip -q ${TOOLS_FOLDER}/${NRF_SDK}.zip -d /vagrant 
fi
grep -q "export NRF5_SDK=/vagrant/${NRF_SDK}" /home/vagrant/.profile || echo "export NRF5_SDK=/vagrant/${NRF_SDK}" >> /home/vagrant/.profile

# Automatically move into the shared folder, but only add the command
# if it's not already there.
grep -q 'cd /vagrant' /home/vagrant/.profile || echo 'cd /vagrant' >> /home/vagrant/.profile

# Touch the marker file so we don't do this again
#touch ${MARKER_FILE}

echo "Xin Chao"
